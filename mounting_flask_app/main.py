# uvicorn main:app --reload

from flask import Flask
from fastapi import FastAPI
from fastapi.middleware.wsgi import WSGIMiddleware

flask_app = Flask(__name__)


@flask_app.route("/")
def index_flask():
    return "Hello World from Flask!"


app = FastAPI()


@app.get("/")
def index():
    return {"message": "Hello World from FastAPI!"}


app.mount("/flask", WSGIMiddleware(flask_app))
