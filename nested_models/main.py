import uvicorn
from typing import Tuple
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class Supplier(BaseModel):
    supplierID: int
    supplierName: str


class Product(BaseModel):
    productID: int
    prodname: str
    price: int
    supp: Supplier


class Customer(BaseModel):
    custID: int
    custname: str
    prod: Tuple[Product]


@app.post("/invoice")
async def get_invoice(c1: Customer):
    return c1


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000, reload=True)
