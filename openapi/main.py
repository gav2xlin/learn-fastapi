# uvicorn main:app --reload

# swagger and redoc urls
# http://127.0.0.1:8000/docs
# http://localhost:8000/redoc

from fastapi import FastAPI

app = FastAPI()


@app.get("/")
async def index():
    return {"message": "Hello World"}
