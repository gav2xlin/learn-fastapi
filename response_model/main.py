import uvicorn
from typing import List
from fastapi import FastAPI
from pydantic import BaseModel, Field

app = FastAPI()


class Student(BaseModel):
    id: int
    name: str = Field(None, title="name of student", max_length=10)
    marks: List[int] = []
    percent_marks: float


class Percent(BaseModel):
    id: int
    name: str = Field(None, title="name of student", max_length=10)
    percent_marks: float


@app.post("/marks", response_model=Percent)
async def get_percent(s1: Student):
    s1.percent_marks = sum(s1.marks) / 2
    return s1


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000, reload=True)
