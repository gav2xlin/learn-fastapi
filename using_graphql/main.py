import uvicorn
import strawberry
from strawberry.asgi import GraphQL
from fastapi import FastAPI


@strawberry.type
class Book:
    title: str
    author: str
    price: int


@strawberry.type
class Query:
    @strawberry.field
    def book(self) -> Book:
        return Book(title="Computer Fundamentals", author="Sinha", price=300)


schema = strawberry.Schema(query=Query)

graphql_app = GraphQL(schema)
app = FastAPI()

app.add_route("/book", graphql_app)
app.add_websocket_route("/book", graphql_app)

if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000, reload=True)
