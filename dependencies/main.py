import uvicorn
from fastapi import FastAPI, Depends, HTTPException

app = FastAPI()


# @app.get("/user/")
# async def user(id: str, name: str, age: int):
#    return {"id": id, "name": name, "age": age}


# async def dependency(id: str, name: str, age: int):
#    return {"id": id, "name": name, "age": age}


class dependency:
    def __init__(self, id: str, name: str, age: int):
        self.id = id
        self.name = name
        self.age = age


async def validate(dep: dependency = Depends(dependency)):
    if dep.age > 18:
        raise HTTPException(status_code=400, detail="You are not eligible")


# @app.get("/user/")
# async def user(dep: dict = Depends(dependency)):
#    return dep


@app.get("/user/", dependencies=[Depends(validate)])
async def user():
    return {"message": "You are eligible"}


@app.get("/admin/")
async def admin(dep: dependency = Depends(dependency)):
    return dep


"""
async def get_db():
   db = DBSession()
      try:
         yield db
      finally:
            db.close()
"""


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000, reload=True)
