import uvicorn
from pymongo import MongoClient
from pydantic import BaseModel
from typing import List
from fastapi import FastAPI, status

client = MongoClient()


class Book(BaseModel):
    bookID: int
    title: str
    author: str
    publisher: str


app = FastAPI()


@app.post("/add_new", status_code=status.HTTP_201_CREATED)
def add_book(b1: Book):
    """Post a new message to the specified channel."""
    with MongoClient() as client:
        book_collection = client[DB][BOOK_COLLECTION]
        result = book_collection.insert_one(b1.dict())
        ack = result.acknowledged
        return {"insertion": ack}


@app.get("/books", response_model=List[str])
def get_books():
    """Get all books in list form."""
    with MongoClient() as client:
        book_collection = client[DB][BOOK_COLLECTION]
        booklist = book_collection.distinct("title")
        return booklist


@app.get("/books/{id}", response_model=Book)
def get_book(id: int):
    """Get all messages for the specified channel."""
    with MongoClient() as client:
        book_collection = client[DB][BOOK_COLLECTION]
        b1 = book_collection.find_one({"bookID": id})
        return b1


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000, reload=True)
