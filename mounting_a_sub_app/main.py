import uvicorn
from fastapi import FastAPI

app = FastAPI()
subapp = FastAPI()


@app.get("/app")
def mainindex():
    return {"message": "Hello World from Top level app"}


@subapp.get("/sub")
def subindex():
    return {"message": "Hello World from sub app"}


app.mount("/subapp", subapp)

if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000, reload=True)
