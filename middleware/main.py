import uvicorn
import time
from fastapi import FastAPI, Request

app = FastAPI()


@app.middleware("http")
async def addmiddleware(request: Request, call_next):
    print("Middleware works!")
    response = await call_next(request)
    return response


@app.get("/")
async def index():
    return {"message": "Hello World"}


@app.get("/{name}")
async def hello(name: str):
    return {"message": "Hello " + name}


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000, reload=True)
