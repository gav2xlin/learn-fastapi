import uvicorn
from fastapi import FastAPI

app = FastAPI()


@app.get("/hello")
async def hello(name: str, age: int):
    return {"name": name, "age": age}


@app.get("/hello/{name}")
async def hello(name: str, age: int):
    return {"name": name, "age": age}


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000, reload=True)
