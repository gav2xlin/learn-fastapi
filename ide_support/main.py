class Rectangle:
    def __init__(self, w: int, h: int) -> None:
        self.width = w
        self.height = h


def area(r: Rectangle) -> int:
    return r.width * r.height


r1 = Rectangle(10, 20)
print("area = ", area(r1))
