import uvicorn
from fastapi import FastAPI, Path, Query

app = FastAPI()


@app.get("/hello/{name}")
async def hello(name: str = Path(..., min_length=3, max_length=10)):
    return {"name": name}


@app.get("/hello/{name}/{age}")
async def hello(
    *,
    name: str = Path(..., min_length=3, max_length=10),
    age: int = Path(..., ge=1, le=100),
    percent: float = Query(..., ge=0, le=100)
):
    return {"name": name, "age": age}


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000, reload=True)
